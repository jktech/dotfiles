#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

# Check

CheckSystem() {
    if [ -f "/etc/arch-release" ]; then
        echo -e "${GREEN} ✓ $1 System is arch ${NC}"
    else
        echo -e "${RED} x System is not arch-linux based ${NC}"
        exit 1
    fi
}

CheckRequiredFiles() {
    files=(.zshrc .p10k.zsh .config .config/hypr .config/waybar .config/dunst .config/wofi .config/fastfetch .local/scripts)

    for file in "${files[@]}"
    do
        if [ -d "$file" ] || [ -f "$file" ]; then
            echo -e "${GREEN} ✓ $file exists${NC}"
        else 
            echo -e "${RED} x $file doesn't exists, make sure that install script in located in dotfiles repository ${NC}"
            exit 1
        fi
    done
}

CheckPackages() {
    commands=(git pacman yay wget curl unzip)

    for comm in "${commands[@]}"
    do
        if ! command -v "${comm}" &> /dev/null; then
            echo -e "${RED} x $comm command was not found on your system, please install it ${NC}"
            exit 1
        else 
            echo -e "${GREEN} ✓ $comm is installed${NC}"
        fi
    done
}

AcceptInstall() {
    read -p "Do you want to install dotfiles? (y/N): " answer

    if [[ ! $answer =~ ^[Yy]$ ]]; then
        echo "Exiting."
        exit 1
    fi
}

AskUser() {
    local question="$1"
    local response
    read -p "$question (Y/n): " response
    response=${response:-Y}
    if [[ "${response^^}" == "Y" ]]; then
        return 0
    else
        return 1
    fi
}

GetBatteries() {
    local BATTERY_DIR="/sys/class/power_supply"
    local batteries=($(ls -1 "$BATTERY_DIR"))

    if [ ${#batteries[@]} -eq 0 ]; then
        return 1
    fi

    echo "List of available batteries:"
    for ((i=0; i<${#batteries[@]}; i++)); do
        local battery_dir="$BATTERY_DIR/${batteries[i]}"

        if [ -f "$battery_dir/model_name" ]; then
                local model_name=$(cat "$battery_dir/model_name")
                local capacity=$(cat "$battery_dir/capacity")
                echo  "${batteries[i]}: $model_name ($capacity%)"
        fi
    done

    return 0
}

ValidateBatteryModel() {
    local BATTERY_DIR="/sys/class/power_supply"
    local model_name="$1"
    if [ -d "$BATTERY_DIR/$model_name" ]; then
        return 0
    else
        return 1
    fi
}


KeepSudo() {
    while true; do
        sudo -v > /dev/null 2>&1
        sleep 240
    done
}

Check() {
    InstallSection "Checking user"

    if [ "$EUID" -eq 0 ]; then
        echo -e "${RED} x Don't run this script as root or sudo ${NC}"
        exit 1;
    else 
        echo -e "${GREEN} ✓ $comm User check${NC}"
    fi

    InstallSection "Checking system"
    CheckSystem

    InstallSection "Checking dependences"
    CheckPackages

    InstallSection "Checking required files"
    CheckRequiredFiles

    InstallSection "Checking user permissions"
    if sudo -v > /dev/null 2>&1; then
        echo -e "${GREEN} ✓ $comm Sudo auth was successful${NC}"
        KeepSudo &
    else
        echo -e "${RED} x $comm Sudo auth was not successful${NC}"
        exit 1
    fi
}

# Install

Install() {
    InstallSection "Updating packages"
    UpdatePackages

    InstallSection "Installing Must-Have"
    InstallMustHave

    InstallSection "Installing fonts"
    InstallFonts

    InstallSection "Installing basic packages"
    InstallBasic

    InstallSection "Installing waybar"
    InstallWaybar

    InstallSection "Installing nautilus"
    InstallNautilus

    InstallSection "Installing terminal"
    InstallTerminal

    InstallSection "Installing kernel"
    InstallKernel
}

InstallSection() {
    echo
    echo "====== $1 ======"
    echo
}

EndSection() {
    echo
    echo -e "${GREEN} ✓ Configuration was successful ${NC}"
    echo
}

UpdatePackages() {
    yay --noconfirm
}

InstallMustHave() {
    sudo pacman -S --noconfirm --needed hyprland sddm kitty qt5-wayland qt6-wayland xdg-desktop-portal-hyprland xdg-dbus-proxy kwallet-pam gnome-keyring kwallet5
}

InstallFonts() {
    sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts adobe-source-han-sans-otc-fonts cantarell-fonts fontconfig gnu-free-fonts gsfonts lib32-fontconfig libfontenc libxfont2 noto-fonts noto-fonts-emoji noto-fonts-extra xorg-font-util xorg-fonts-100dpi xorg-fonts-75dpi xorg-fonts-encodings xorg-mkfontscale ttf-jetbrains-mono-nerd ttf-fira-code ttf-firacode-nerd ttf-dejavu ttf-font-awesome ttf-firacode-nerd ttf-carlito ttf-liberation ttf-opensans
    yay -S --noconfirm --needed ttf-times-new-roman
}

InstallBasic() {
    sudo pacman -S --noconfirm hyprpaper hyprland-qtutils hyprpicker hyprutils hyprpolkitagent hyprlock hyprcursor hypridle dunst cliphist fastfetch wofi xdg-user-dirs pavucontrol
    yay -S --noconfirm grimblast-git
}

InstallNautilus() {
    if AskUser "Do you want to install nautilus (gnome file manager)?"; then
        sudo pacman -S --noconfirm nautilus python-nautilus file-roller
        yay -S --noconfirm nautilus-open-any-terminal nautilus-copy-path
    fi
}

InstallWaybar() {
    sudo pacman -S --noconfirm waybar
    yay -S --noconfirm waybar-module-pacman-updates-git hyprland-autoname-workspaces-git
}

InstallTerminal() {
    sudo pacman -S --noconfirm zsh
    sudo pacman -S --noconfirm zsh-syntax-highlighting zsh-autosuggestions lsd
    yay -S --noconfirm ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k-git oh-my-zsh-git
}

InstallKernel() {
    if AskUser "Do you want to install zen kernel?"; then 
        sudo pacman -S --noconfirm linux-zen linux-zen-headers
        sudo grub-mkconfig -o /boot/grub/grub.cfg
    fi

    if AskUser "Do you want to install lts kernel?"; then 
        sudo pacman -S --noconfirm linux-lts linux-lts-headers
        sudo grub-mkconfig -o /boot/grub/grub.cfg
    fi
}

InstallOptional() {
    if AskUser "Do you want to start optional utility wizard?"; then 
        local items=("visual-studio-code-bin" "spotify-launcher" discord "gnome-calculator" "gnome-text-editor" "vlc" "gimp" "gamemode" "obsidian" "firewalld" "eog")
        local selected=""

        for value in "${items[@]}"; do
            read -p "Do you want to install '$value'? (y/N): " option

            if [ "$option" = "Y" ] || [ "$option" = "y" ]; then
                selected="${selected} ${value}"
            fi
        done

        if [ -z "$selected" ]; then
            echo "No optional package was selected"
        else
            echo "Installing optional packages:"
            echo "$selected"
            yay -S --noconfirm $selected
        fi
    fi
}

# Configure

Configure() {
    InstallSection "Configuring hyprland"
    ConfigureHyprland
    EndSection

    InstallSection "Configuring themes"
    ConfigureThemes
    EndSection

    InstallSection "Configuring permissions"
    ConfigurePermissions
    EndSection

    InstallSection "Configuring waybar"
    ConfigureWaybar
    EndSection

    InstallSection "Configuring hypridle"
    ConfigureHypridle
    EndSection

    InstallSection "Configuring wofi"
    ConfigureWofi
    EndSection

    InstallSection "Configuring dunst"
    ConfigureDunst
    EndSection

    InstallSection "Configuring kitty"
    ConfigureKitty
    EndSection

    InstallSection "Setting wallpaper"
    ConfigureWallpaper
    EndSection

    InstallSection "Setting shell"
    ConfigureShell
    EndSection

    if ! command -v nautilus &> /dev/null; then
        InstallSection "Setting nautilus"
        ConfigureNautilus
        EndSection
    fi

    InstallSection "Setting grub"
    ConfigureGrub
    EndSection

    InstallSection "Setting SDDM"
    ConfigureSDDM
    EndSection

    InstallSection "Setting bluetooth"
    ConfigureBluetooth
    EndSection

    InstallSection "Setting fastfetch"
    ConfigureFastfetch
    EndSection

    InstallSection "Setting network applet"
    ConfigureNmapplet
    EndSection
}

ConfigureHyprland() {
    mkdir -p ~/.config/hypr/
    echo "Copying hyprland config files"
    cp -r .config/hypr/* ~/.config/hypr/
    echo "Setting hyprland scripts as executable"
    chmod +x ~/.config/hypr/autostart.sh

    echo "Cloning catppuccin theme from github"
    git clone --quiet https://github.com/catppuccin/hyprland.git /tmp/catppuccin-hyprland/

    echo "Copying theme file into hyprland config directory"
    cp /tmp/catppuccin-hyprland/themes/mocha.conf ~/.config/hypr/

    echo "Linking theme in hyprland config file"
    sed -i '/# source = ~/.config\/hypr\/mocha.conf/source = ~/.config\/hypr\/mocha.conf/' ~/.config/hypr/hyprland.conf

    echo "Copying scripts to local directory"
    mkdir -p ~/.local/scripts/
    cp -r .local/scripts/* ~/.local/scripts/

    echo "Setting scripts as executable"
    chmod u+x ~/.local/scripts/*.sh

    echo "Creating user directories"
    LC_ALL=C.UTF-8 xdg-user-dirs-update --force
}

ConfigureHypridle() {
    if AskUser "Do you want to add install brightnessctl? (Control device brightness like keyboard, display)"; then
        if AskUser "Do you want to add a listener for display backlight in hypridle?"; then
            echo -e "listener {" >> ~/.config/hypr/hypridle.conf
            echo -e "\ttimeout = 120                                # 2min." >> ~/.config/hypr/hypridle.conf
            echo -e "\ton-timeout = brightnessctl -s set 10         # set monitor backlight to minimum, avoid 0 on OLED monitor." >> ~/.config/hypr/hypridle.conf
            echo -e "\ton-resume = brightnessctl -r                 # monitor backlight restore." >> ~/.config/hypr/hypridle.conf
            echo -e "}" >> ~/.config/hypr/hypridle.conf
        fi

        if AskUser "Do you want to add a listener for keyboard backlight in hypridle?"; then
            echo -e "\n# turn off keyboard backlight, uncomment this section if have keyboard backlight." >> ~/.config/hypr/hypridle.conf
            echo -e "listener {"  >> ~/.config/hypr/hypridle.conf
            echo -e "\ttimeout = 120                                                       # 2min." >> ~/.config/hypr/hypridle.conf
            echo -e "\ton-timeout = brightnessctl -s --device='*::kbd_backlight' set 0     # turn off keyboard backlight." >> ~/.config/hypr/hypridle.conf
            echo -e "\ton-resume = brightnessctl -r --device='*::kbd_backlight'            # turn on keyboard backlight." >> ~/.config/hypr/hypridle.conf
            echo -e "}" >> ~/.config/hypr/hypridle.conf
        fi

    else
        echo "Removing backlight module from waybar"
        jq 'del(.["modules-right"][] | select(. == "backlight"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
    fi
}

ConfigurePermissions() {
    echo "Adding user to video group"
    sudo usermod -aG video ${USER}
    echo "Adding user to input group"
    sudo usermod -aG input ${USER}
}

ConfigureKitty() {
    echo "Copying kitty config files"
    mkdir -p ~/.config/kitty/
    cp -r .config/kitty/* ~/.config/kitty/
}

ConfigureWaybar() {
    mkdir -p ~/.config/waybar/
    cp -r .config/waybar/* ~/.config/waybar/

    mkdir -p ~/.config/hyprland-autoname-workspaces/
    cp -r .config/hyprland-autoname-workspaces/* ~/.config/hyprland-autoname-workspaces/

    if GetBatteries; then
        local selectedBattery
        while true; do
            read -p "Which battery you want to display in the waybar (N, model): " selectedBattery
            if [ -z "$selectedBattery" ] || [ "${selectedBattery,,}" == "n" ]; then
                jq 'del(.["modules-right"][] | select(. == "battery"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
                echo "No battery will be shown in waybar"
                break

            elif ValidateBatteryModel "$selectedBattery"; then
                jq --arg new_value "$selectedBattery" '.battery.bat = $selectedBattery' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
                break
            else
                echo "Enter valid battery e.g BAT0, hidpp_battery_0..."
            fi
        done

    else
        jq 'del(.["modules-right"][] | select(. == "battery"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
        echo "Battery was not found in your computer, if you want to display battery status in waybar, edit waybar config manualy"
    fi
}

ConfigureWofi() {
    echo "Copying wofi config files"
    mkdir -p ~/.config/wofi/
    cp -r .config/wofi/* ~/.config/wofi/
}

ConfigureDunst() {
    echo "Copying dunst config files"
    mkdir -p ~/.config/dunst/
    cp -r .config/dunst/* ~/.config/dunst/
}

ConfigureSDDM() {
    echo "Enabling sddm service"
    sudo systemctl enable sddm

    if AskUser "Do you want to install catppuccin mocha SDDM theme?"; then
        echo "Creating sddm config directory"
        sudo mkdir -p /etc/sddm.conf.d/
        echo "Copying default sddm config into config directory"
        sudo cp /usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/sddm.conf

        echo "Installing dependencies for theme"
        sudo pacman -Syu --noconfirm --needed qt5-svg qt5-quickcontrols2
        echo "Downloading SDDM theme from github"
        wget -q 'https://github.com/catppuccin/sddm/releases/latest/download/catppuccin-mocha.zip' -O '/tmp/catppuccin-mocha.zip.tmp'

        echo "Creating theme directory"
        sudo mkdir -p  /usr/share/sddm/themes/
        
        echo "Copying theme archive into config directory"
        sudo cp /tmp/catppuccin-mocha.zip.tmp /usr/share/sddm/themes/catppuccin-mocha.zip
        echo "Extracting archive"
        sudo unzip -qq /usr/share/sddm/themes/catppuccin-mocha.zip -d /usr/share/sddm/themes/
        echo "Removing original theme archive"
        sudo rm -f /usr/share/sddm/themes/catppuccin-mocha.zip
        echo "setting theme in config file"
        sudo sed -i '/^\[Theme\]/,/^\[/ s/^Current=.*/Current=catppuccin-mocha/' /etc/sddm.conf.d/sddm.conf
    fi
}

ConfigureGrub() {
    if AskUser "Do you want to install catppuccin mocha GRUB theme?"; then
        echo "Cloning repo from github"
        git clone --quiet https://github.com/catppuccin/grub.git /tmp/grub/
        echo "Copying theme files"
        sudo cp -r /tmp/grub/src/* /usr/share/grub/themes/

        echo "Writing to config file"
        grub_theme_path="/usr/share/grub/themes/catppuccin-mocha-grub-theme/theme.txt"
        sudo sed -i 's|#\?GRUB_THEME=.*|GRUB_THEME="'$grub_theme_path'"|' /etc/default/grub

        echo "Generating GRUB config"
        sudo grub-mkconfig -o /boot/grub/grub.cfg
    fi
}

ConfigureShell() {
    echo "Copying zsh config files"
    cp -r .zshrc ~/.zshrc
    cp -r .p10k.zsh ~/.p10k.zsh

    echo "Copying lsd config files"
    mkdir -p  ~/.config/lsd/
    cp -r .config/lsd/* ~/.config/lsd/

    echo "Setting zsh as default shell"
    chsh -s /usr/bin/zsh
}

ConfigureNautilus() {
    gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal kitty
}

ConfigureWallpaper() {
    if AskUser "Do you want to download Plasma mountains as wallpaper and hyprlock background?"; then
        wallpaper_path='/usr/share/backgrounds/wallpaper.jpg'
        echo "Downloading wallpaper"

        wget -q 'https://raw.githubusercontent.com/orangci/walls-catppuccin-mocha/master/cool.jpg' -O '/tmp/wallpaper.jpg.tmp'
        
        sudo mkdir -p /usr/share/backgrounds/

        echo "Moving wallpaper file to wallpaper directory"
        sudo mv /tmp/wallpaper.jpg.tmp "$wallpaper_path"

        echo "Writing wallpaper path to hyprpaper config file"

        echo -e "\npreload = ${wallpaper_path}\n\n" >> ~/.config/hypr/hyprpaper.conf
        echo -e "\nwallpaper = ,${wallpaper_path}\n" >> ~/.config/hypr/hyprpaper.conf

    else 
        echo "You can configure your own wallpaper in file: ~/.config/hypr/hyprpaper.conf"
    fi

    if AskUser "Do you want to disable IPC for hyprpaper, which can increase battery life?"; then 
        echo -e "\nipc = off\n" >> ~/.config/hypr/hyprpaper.conf
    else 
        echo -e "\nipc = on\n" >> ~/.config/hypr/hyprpaper.conf
    fi

}

ConfigureThemes() {
    echo "Copying gtk 2.0 config file"
    cp -r .gtkrc-2.0 ~/.gtkrc-2.0

    echo "Copying gtk 3.0 config file"
    mkdir -p ~/.config/gtk-3.0/
    cp -r .config/gtk-3.0/* ~/.config/gtk-3.0/

    echo "Setting \"prefer dark\" option for gnome apps"
    gsettings set org.gnome.desktop.interface color-scheme prefer-dark

    if AskUser "Do you want to install Tela icon theme from AUR? (It takes a long time)"; then
        yay -S --noconfirm --needed tela-circle-icon-theme-git

        echo "Setting theme into GTK config files"
        echo 'gtk-icon-theme-name="Tela-circle-blue-dark"' >> ~/.gtkrc-2.0
        echo 'gtk-icon-theme-name=Tela-circle-blue-dark' >> ~/.config/gtk-3.0/settings.ini
    fi

    if AskUser "Do you want to install Catppuccin GTK mocha from AUR?"; then
        yay -S --noconfirm --needed catppuccin-gtk-theme-mocha

        echo "Setting theme into GTK config files"
        echo 'gtk-theme-name="catppuccin-mocha-blue-standard+default"' >> ~/.gtkrc-2.0
        echo 'gtk-theme-name=catppuccin-mocha-blue-standard+default' >> ~/.config/gtk-3.0/settings.ini

        echo "Setting env variable for theme"
        echo -e '\nenv = GTK_THEME,catppuccin-mocha-blue-standard+default' | sudo tee -a ~/.config/hypr/env.conf
        echo -e '\nGTK_THEME=catppuccin-mocha-blue-standard+default' | sudo tee -a /etc/environment # For nautilus
    fi

    if AskUser "Do you want to import Adwaita cursors for hyprcursor?"; then
        echo "Installing required packages"
        sudo pacman -S --noconfirm --needed xcur2png

        echo "Downloading adwaita cursors from github (It may take some time)"
        wget -q https://github.com/manu-mannattil/adwaita-cursors/releases/latest/download/adwaita-cursors.tar.gz -O /tmp/adwaita-cursors.tar.gz

        echo "Extracting cursors archive"
        tar -xvzf /tmp/adwaita-cursors.tar.gz

        echo "Extracting Adwaita XCursors"
        hyprcursor-util --extract hyprcursor-util --extract /tmp/adwaita-cursors/Adwaita
    fi

    if AskUser "Do you want to install kvantum, qt5ct and qt6ct (Utility for QT themes)?"; then
        sudo pacman -S --noconfirm --needed kvantum qt6ct qt5ct

        echo -e '\nenv = QT_QPA_PLATFORMTHEME, qt6ct' | sudo tee -a ~/.config/hypr/env.conf

        echo "!!! Is required to manualy download catppuccin kvantum theme from git and install it using kvantum manager !!!"
    fi
}

ConfigureBluetooth() {
    if rfkill list | grep -i bluetooth > /dev/null; then
        if AskUser "Do you want to configure bluetooth? (Enable and install blueman)"; then
            sudo pacman -S --noconfirm blueman
            sudo systemctl enable --now bluetooth.service
        else
            jq 'del(.["modules-left"][] | select(. == "bluetooth"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
        fi
    else
        jq 'del(.["modules-left"][] | select(. == "bluetooth"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
    fi
}

ConfigureNmapplet() {
    if AskUser "Do you want to install NetworkManager applet (Utility for network configuration)?"; then
        sudo pacman -S --noconfirm network-manager-applet

        echo "Setting autostart for nm-applet"
        echo -e "\nexec-once = nm-applet --indicator\n" >> ~/.config/hypr/autostart.conf
    fi
}

ConfigureFastfetch() {
    echo "Copying fastfetch config files"
    mkdir -p ~/.config/fastfetch/
    cp -r .config/fastfetch/* ~/.config/fastfetch/
}

ConfigureOptional() {
    # if command -v "brave" &> /dev/null; then
    #     echo "== Brave =="
    #     echo "Copying brave incognito desktop file"
    #     mkdir -p ~/.local/share/applications/
    #     cp .local/share/applications/brave-browser-incognito.desktop ~/.local/share/applications/brave-browser-incognito.desktop
    # fi

    if ! command -v "gamemoderun" &> /dev/null; then
        echo "== Gamemode =="
        echo "Removing gamemode from waybar"
        jq 'del(.["modules-left"][] | select(. == "gamemode"))' ~/.config/waybar/config > tmpfile && mv tmpfile ~/.config/waybar/config
    fi

    if command -v "firewalld" &> /dev/null; then
        sudo systemctl enable --now firewalld
    fi
}

#Finish

FinishInstall() {
    InstallSection "Optional packages"
    InstallOptional

    InstallSection "Setting optional"
    ConfigureOptional

    InstallSection "Installation is finished"
    if AskUser "Do you want to reboot computer?"; then 
        sudo systemctl reboot
        exit 0
    fi

    if AskUser "Do you want to start SDDM now?"; then 
        sudo systemctl start sddm
        exit 0
    fi
}

main() {
    clear
    InstallSection "Installing JKtech dotfiles"

    Check

    echo

    AcceptInstall

    echo

    Install

    echo

    Configure

    echo

    FinishInstall
}

main