#!/bin/bash

# Seznam HTTP kódů a jejich textů
options=(
    "export TERM=xterm"
)

# Zobrazení wofi a výběr kódu
selected=$(printf '%s\n' "${options[@]}" | wofi --dmenu --prompt="Select text:")

# Kontrola, zda byl něco vybráno
if [ -n "$selected" ]; then
    echo -n "$selected" | wl-copy
fi