# Popis

Thanks to [Guanran928](https://github.com/Guanran928/dotfiles/tree/main) <span style="color:red">♥</span> for inspiration

- [EndeavourOS](https://endeavouros.com/) / [Arch linux](https://archlinux.org/)
- [Hyprland](https://hyprland.org/)

![Screenshot](/assets/screenshot_1.png) 
![Screenshot](/assets/screenshot_2.png)

# Automatická instalace

V projektu je připravený instalační skript, který nainstaluje a nastaví všechny balíky a programy. Pokud v průběhu instalace dojde k problému, script vypíše jednotlivé instalační kroky, takže pokud dojde k problému, je možné pokračovat manuálně od posledního provedeného kroku. **Script nemůže být přesunut pryč z adresáře dotfiles, jinak se instalace nezdaří**

```
./install.sh
```

## Aktualizace konfigurace

Pokud chcete aktualizovat dotfiles na konfiguraci z gitu, můžete použít script. Před aktualizací si můžete zálohovat aktuální konfig. Script neaktualizuje všechny konfigurační soubory, protože jsou specifické pro každý počítač. Script **neinstaluje** potřebné závislosti.

```
./update.sh
```

# Manuální Instalace

## [Must-Have](https://wiki.hyprland.org/Useful-Utilities/Must-have/)

```bash
sudo pacman -S hyprland sddm kitty qt5-wayland qt6-wayland xdg-desktop-portal-hyprland kwallet-pam gnome-keyring kwallet5
```

Povolení *SDDM*

```bash
sudo systemctl enable --now sddm
```

Poté se přihlásíme do Hyprlandu

## Instalace fontů

```bash
sudo pacman -S --needed adobe-source-han-sans-otc-fonts gnu-free-fonts gsfonts lib32-fontconfig noto-fonts noto-fonts-emoji xorg-font-util xorg-fonts-100dpi xorg-fonts-75dpi xorg-mkfontscale ttf-jetbrains-mono-nerd ttf-firacode-nerd ttf-fira-code ttf-font-awesome ttf-firacode-nerd ttf-carlito ttf-liberation ttf-opensans && yay -S --needed ttf-times-new-roman
```

## Instalace základních balíků

- hyprpaper - Tapeta
- hyprlock - Lock utility
- hypridle - Démon pro neaktivitu
- hyprcursor - Kursor pro hyprland
- hyprpolkitagent - Autentifikační okno
- hyprpicker - Color picker, potřebné pro grimblast
- brightnessctl - Nastavení jasu
- dunst - Zobrazení notifikací
- cliphist - Kopírování do schránky
- nautilus - Prohlížeč souborů
- fastfetch - Neofetch ale rychlejší
- wofi - App launcher
- xdg-user-dirs - Uživatelské adresáře
- pavucontrol - Konfigurace audio zařízení


```bash
sudo pacman -S brightnessctl hyprutils hyprland-qtutils hyprpaper hyprpicker hyprcursor hyprpolkitagent hyprlock hypridle dunst cliphist nautilus fastfetch wofi xdg-user-dirs
```

### AUR Aplikace

- grimblast - Nástroj pro screenshoty

```bash
yay -S grimblast-git
```

### Povolení konfiurace jasu a klávesových zkratek pro uživatele

```bash
sudo usermod -aG video ${USER}
sudo usermod -aG input ${USER}
```

## Waybar

Instalace waybaru:

```bash
sudo pacman -S waybar
```

Moduly:

- waybar-module-pacman-updates-git - Zobrazí počet balíků k aktualizaci

```bash
yay -S waybar-module-pacman-updates-git
```

# Témata

Používám [Catppuccin témata](https://github.com/catppuccin/)

## Tapeta
[Odkaz](https://raw.githubusercontent.com/orangci/walls-catppuccin-mocha/master/cool.jpg)

## Ikony

Používám [Tela icon theme](https://github.com/vinceliuice/Tela-icon-theme)

```bash
yay -S tela-circle-icon-theme-blue
```

## GTK Aplikace

### Preferování tmavého téma

```bash
gsettings set org.gnome.desktop.interface color-scheme prefer-dark
```

### Catppuccin gtk mocha

Používám [Catppuccin gtk mocha](https://github.com/catppuccin/gtk)

```bash
yay -S catppuccin-gtk-theme-mocha
```

#### Utilita pro nastavování GTK témat

```bash
yay -S nwg-look
```

**Některé aplikace nerespektují toto nastavení, proto je potřeba nastavit téma i jako environment variable `GTK_THEME` v souboru `/etc/environment`**

## KDE Aplikace

- kvantum - Témata pro KDE aplikace

```bash
sudo pacman -S kvantum
```

## Téma pro SDDM

Vytvoříme složku a nakopírujeme defaultní konfiguraci

```bash
sudo mkdir -p /etc/sddm.conf.d/
sudo cp /usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/sddm.conf
```

Naimportujeme téma do složky `/usr/share/sddm/themes/` a vložíme název témata do konfiguračního souboru

```bash
nano /etc/sddm.conf.d/sddm.conf
```

## Pacman

V konfiguračním souboru pacman v sekci *options* odkomentujeme *Color* a pod tento řádek přidáme *ILoveCandy*.

```bash
sudo nano /etc/pacman.conf
```

## Terminál

#### Instalace a konfigurace zsh

```bash
sudo pacman -S zsh
```

Spustíme zsh a vybereme možnost **0**

```bash
zsh
```

#### Instalace Oh My zsh
```
yay -S oh-my-zsh-git
```

#### Instalace powerlevel10k

```bash
yay -S ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k
```

#### Instalace puginů

```bash
sudo pacman -S zsh-syntax-highlighting zsh-autosuggestions
```

Do souboru `.zshrc` nalinkujeme soubory

```
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
```

`ZSH_AUTOSUGGEST_STRATEGY=( history completion )`

## Gamemode
Instalace gamemode

```bash
sudo pacman -S gamemode
```

### Omezení efektů pro hyprland během hraní:
Zkopírujeme scripty do složky `~/.local/scripts/`

```bash
mkdir -p ~/.local/scripts/
cp .local/scripts/start-gamemode.sh .local/scripts/end-gamemode.sh ~/.local/scripts/
chmod +x ~/.local/scripts/start-gamemode.sh ~/.local/scripts/end-gamemode.sh
```

Použijeme vzorový konfigurační soubor z [githubu](https://github.com/FeralInteractive/gamemode/blob/master/example/gamemode.ini) a vložíme obsah do souboru `~/.config/gamemode.ini`. Do sekce **\[custom\]** přidáme tyto dva řádky: 

```ini
start=~/.local/scripts/start-gamemode.sh
end=~/.local/scripts/end-gamemode.sh
```
### Povolení gamemode pro steam
V launch options přidáme:
```
gamemoderun %command%
```


## LSD

[LSD](https://github.com/lsd-rs/lsd) je přepracovaná verze ls, která umožňuje konfiguraci a stylování


Instalace:
```bash
sudo pacman -S lsd
```

V souboru .zshrc přidáme tyto aliasy: 
```zshrc
alias ls='lsd'
```

Pokud chceme používat další aliasy:
```
alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'
```

## Nautilus
#### Open in any terminal

```bash
yay -S nautilus-open-any-terminal
```

Konfigurace

```bash
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal kitty
```

#### Image converter

```bash
sudo pacman -S imagemagick nautilus-image-converter
```

#### MTP (Android file transfer)

```bash
sudo pacman -S gvfs-mtp
```

#### Znovu načíst terminál

```bash
nautilus -q
```

## Konfigurace sítě

Pro správu sítě můžeme použít applet pro *NetworkManager*, který se zobrazí v system tray (*Waybar*)

### Instalace

```bash
sudo pacman -S network-manager-applet
```

### Povolení po spuštění

Do konfiguračního souboru pro hyprland přidáme tento řádek

```conf
exec-once = nm-applet --indicator
```

## Fix pro aplikace

### Mailspring

Pro použití gnome-libsecret upravíme hodnotu *Exec* v souboru `/usr/share/applications/Mailspring.desktop`

```
Exec=mailspring --password-store="gnome-libsecret"
```

Nebo nastavíme autostart pro *hyprland*:

```
exec-once = mailspring --password-store="gnome-libsecret" --background
```

## Brave

Na stránce

**brave://flags/**

Nastavit *Preferred Ozone platform* na **Wayland** nebo **Auto**

## Libreoffice
Pro nainstalování spellcheck
```bash
yay -S hunspell-cs
yay -S hunspell-en_us
```

### TODO
- [ ] EN Readme
- [ ] Templates
- [ ] Return to rofi??
- [ ] Allow multilib