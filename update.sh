#!/bin/bash

AskUser() {
    local question="$1"
    local response
    read -p "$question (Y/n): " response
    response=${response:-Y}
    if [[ "${response^^}" == "Y" ]]; then
        return 0
    else
        return 1
    fi
}

InstallSection() {
    echo
    echo "====== $1 ======"
    echo
}

CompareDirs() {
    local dir1="$1"
    local dir2="$2"

    for file1 in "$dir1"/*
    do
        name=$(basename "$file1")
        if [ -f "$dir2/$name" ]; then
            if cmp -s "$file1" "$dir2/$name"; then
                return 1
            fi
        else
            return 1
        fi
    done

    return 0
}

AddMissingLines() {
    local file1="$1"
    local file2="$2"
    local tmp_file="tmp_file.txt"

    cat "$file1" > "$tmp_file"
    echo -e "\n# ==== Custom values ====\n" >>  "$tmp_file"

    while IFS= read -r line; do
        if [[ ! "$line" =~ ^# ]]; then
            found=$(grep -cFx "$line" "$file2")
            if [ "$found" -eq 0 ]; then
                echo "$line" >> "$tmp_file"
            fi
        fi
    done < "$file1"

    mv "$tmp_file" "$file2"
}

CompareWaybarConfig() {
    # Compare css
    if ! cmp -s ".config/waybar/style.css" "/home/$USER/.config/waybar/style.css"; then
        return 1
    fi

    # Compare config
    cp .config/waybar/config /tmp/waybarconf_original
    jq 'del(.["modules-left"], .["modules-right"], .["modules-center"], .battery.bat)' /tmp/waybarconf_original > /tmp/waybarconf_original.tmp && mv /tmp/waybarconf_original.tmp /tmp/waybarconf_original

    cp /home/$USER/.config/waybar/config /tmp/waybarconf_new
    jq 'del(.["modules-left"], .["modules-right"], .["modules-center"], .battery.bat)' /tmp/waybarconf_new > /tmp/waybarconf_new.tmp && mv /tmp/waybarconf_new.tmp /tmp/waybarconf_new

    if ! cmp -s "/tmp/waybarconf_new" "/tmp/waybarconf_original"; then
        return 1
    fi

    return 0
}

CompareHyprlandConfig() {
    if [[ ! -z $(comm -23 <(sort .config/hypr/autostart.conf) <(sort /home/$USER/.config/hypr/autostart.conf)) ]]; then
        comm -23 <(sort .config/hypr/autostart.conf) <(sort /home/$USER/.config/hypr/autostart.conf)
        echo "Diff in autostart.conf"
        return 1
    fi

    if [[ ! -z $(comm -23 <(sort .config/hypr/env.conf) <(sort /home/$USER/.config/hypr/env.conf)) ]]; then
        echo "Diff in env.conf"
        return 1
    fi

    if [[ ! -z $(comm -23 <(sort .config/hypr/hyprlock.conf) <(sort /home/$USER/.config/hypr/hyprlock.conf)) ]]; then
        echo "Diff in hyprlock.conf"
        return 1
    fi

    if ! cmp -s ".config/hypr/autostart.sh" "/home/$USER/.config/hypr/autostart.sh"; then
        echo "Diff in autostart.sh"
        return 1
    fi

    if ! cmp -s ".config/hypr/keybinds.conf" "/home/$USER/.config/hypr/keybinds.conf"; then
        echo "Diff in keybinds.conf"
        return 1
    fi

    if ! cmp -s ".config/hypr/windowrules.conf" "/home/$USER/.config/hypr/windowrules.conf"; then
        echo "Diff in windowrules.conf"
        return 1
    fi
}

CreateBackup() {
    if AskUser "Do you want to backup your current config?"; then
        mkdir -p Backups

        echo "Creating tmp directory for backup"
        mkdir -p Backups/tmp/

        echo "Copying dunst files"
        mkdir -p Backups/tmp/dunst/
        cp -r ~/.config/dunst/* Backups/tmp/dunst/

        echo "Copying gtk files"
        mkdir -p Backups/tmp/gtk-3.0/
        cp -r ~/.config/gtk-3.0/* Backups/tmp/gtk-3.0/
        cp  ~/.gtkrc-2.0 Backups/tmp/

        echo "Copying hyprland files"
        mkdir -p Backups/tmp/hypr/
        cp -r ~/.config/hypr/* Backups/tmp/hypr/

        echo "Copying hyprland autoname workspaces files"
        mkdir -p Backups/tmp/hyprland-autoname-workspaces/
        cp -r ~/.config/hyprland-autoname-workspaces/* Backups/tmp/hyprland-autoname-workspaces/

        echo "Copying kitty files"
        mkdir -p Backups/tmp/kitty/
        cp -r ~/.config/kitty/* Backups/tmp/kitty/

        echo "Copying lsd files"
        mkdir -p Backups/tmp/lsd/
        cp -r ~/.config/lsd/* Backups/tmp/lsd/

        echo "Copying waybar files"
        mkdir -p Backups/tmp/waybar/
        cp -r ~/.config/waybar/* Backups/tmp/waybar/

        echo "Copying wofi files"
        mkdir -p Backups/tmp/wofi/
        cp -r ~/.config/wofi/* Backups/tmp/wofi/

        echo "Copying zsh files"
        cp -r ~/.zshrc Backups/tmp/
        cp -r ~/.p10k.zsh Backups/tmp/

        echo "Copying fastfetch files"
        mkdir -p Backups/tmp/fastfetch/
        cp -r ~/.config/fastfetch/* Backups/tmp/fastfetch/

        echo "Creating backup archive"
        current_time=$(date +"%d.%m.%Y-%H:%M")
        zip -qq -r "Backups/Backup-$current_time.zip" Backups/tmp

        echo "Deleting tmp files"
        rm -r Backups/tmp

    else
        echo "No backup will be created"
    fi
}

UpdateDunst() {
    if CompareDirs ".config/dunst/" "/home/$USER/.config/dunst/"; then
        if AskUser "Do you want update dunst?"; then
            mkdir -p ~/.config/dunst/
            cp -r .config/dunst/* ~/.config/dunst/
        fi
    else
        echo "Dunst config is up to date"
    fi
}

UpdateKitty() {
    if CompareDirs ".config/kitty/" "/home/$USER/.config/kitty/"; then
        if AskUser "Do you want update kitty?"; then
            mkdir -p ~/.config/kitty/
            cp -r .config/kitty/* ~/.config/kitty/
        fi
    else
        echo "Kitty config is up to date"
    fi
}

UpdateHyprland() {
    if ! CompareHyprlandConfig; then
        if AskUser "Do you want update hyprland?"; then
            AddMissingLines .config/hypr/windowrules.conf ~/.config/hypr/windowrules.conf
            AddMissingLines .config/hypr/env.conf ~/.config/hypr/env.conf
            AddMissingLines .config/hypr/keybinds.conf ~/.config/hypr/keybinds.conf

            cp .config/hypr/autostart.sh ~/.config/hypr/
            chmod +x ~/.config/hypr/autostart.sh

            echo "Reloading hyprland"
            hyprctl reload
        fi

    else
        echo "Hyprland config is up to date"
    fi
}

UpdateWorspaces() {
    if CompareDirs ".config/hyprland-autoname-workspaces/" "/home/$USER/.config/hyprland-autoname-workspaces/"; then
        if AskUser "Do you want update hyprland autoname workspaces?"; then
            mkdir -p ~/.config/hyprland-autoname-workspaces/
            cp -r .config/hyprland-autoname-workspaces/* ~/.config/hyprland-autoname-workspaces/

            if [ $XDG_SESSION_TYPE == "wayland" ] ; then
                echo "Reloading workspaces"
                killall hyprland-autoname-workspaces
                hyprland-autoname-workspaces & disown
            fi
        fi
    else
        echo "Workspaces are up to date"
    fi
}

UpdateWaybar() {
    if ! CompareWaybarConfig; then
        if AskUser "Do you want update waybar?"; then
            mkdir -p ~/.config/waybar/
            cp -r .config/waybar/style.css ~/.config/waybar/

            if [ -f "/home/$USER/.config/waybar/config" ]; then
                echo "Editing existing config"
                mv ~/.config/waybar/config ~/.config/waybar/config.old
                cp .config/waybar/config ~/.config/waybar/

                JSON_INPUT_FILE="/home/$USER/.config/waybar/config.old"
                JSON_OUTPUT_FILE="/home/$USER/.config/waybar/config"

                # Copy modules
                jq --argjson modules_left "$(jq '.["modules-left"]' "$JSON_INPUT_FILE")" '.["modules-left"]=$modules_left' "$JSON_OUTPUT_FILE" > temp_output.json
                jq --argjson modules_center "$(jq '.["modules-center"]' "$JSON_INPUT_FILE")" '.["modules-center"]=$modules_center' "$JSON_OUTPUT_FILE" > temp_output.json
                jq --argjson modules_right "$(jq '.["modules-right"]' "$JSON_INPUT_FILE")" '.["modules-right"]=$modules_right' "$JSON_OUTPUT_FILE" > temp_output.json

                # Copy battery value
                jq --argjson bat_name "$(jq '.battery.bat' "$JSON_INPUT_FILE")" '.battery.bat=$bat_name' "$JSON_OUTPUT_FILE" > temp_output.json
                mv temp_output.json "$JSON_OUTPUT_FILE"

                # Remove original config
                rm "$JSON_INPUT_FILE"
            else
                echo "Copying new config"
                cp -r .config/waybar/config ~/.config/waybar/
            fi

            if [ $XDG_SESSION_TYPE == "wayland" ] ; then
                echo "Reloading waybar"
                killall waybar
                waybar & disown
            fi
        fi
    else
        echo "Waybar config is up to date"
    fi
}

UpdateLsd() {
    if CompareDirs ".config/lsd/" "/home/$USER/.config/lsd/"; then
        if AskUser "Do you want update lsd?"; then
            mkdir -p ~/.config/lsd/
            cp -r .config/lsd/* ~/.config/lsd/
        fi
    else
        echo "LSD config is up to date"
    fi
}

UpdateWofi() {
    if CompareDirs ".config/lsd/" "/home/$USER/.config/lsd/"; then
        if AskUser "Do you want update wofi?"; then
            mkdir -p ~/.config/wofi/
            cp -r .config/wofi/* ~/.config/wofi/
        fi
    else 
        echo "Wofi config is up to date"
    fi
}

UpdateZsh() {
    if ! cmp -s ".zshrc" "/home/$USER/.zshrc" || ! cmp -s ".p10k.zsh" "/home/$USER/.p10k.zsh"; then
        if AskUser "Do you want update zsh?"; then
            echo "To apply new config, restart all terminal apps"
            cp .zshrc ~/.zshrc
            cp .p10k.zsh ~/.p10k.zsh
        fi
    else
        echo "ZSH config is up to date"
    fi
}

UpdateFastFetch() {
    if CompareDirs ".config/fastfetch/" "/home/$USER/.config/fastfetch/"; then
        if AskUser "Do you want update fastfetch?"; then
            mkdir -p ~/.config/fastfetch/
            cp -r .config/fastfetch/* ~/.config/fastfetch/
        fi
    else
        echo "fastfetch config is up to date"
    fi
}

UpdateScripts() {
    if CompareDirs ".local/scripts/" "/home/$USER/.local/scripts/"; then
        if AskUser "Do you want update scripts?"; then
            mkdir -p ~/.local/scripts/
            cp .local/scripts/* ~/.local/scripts/
        fi
    else
        echo "Scripts are up to date"
    fi
}

Main() {
    InstallSection "Backup"
    CreateBackup
    
    InstallSection "Hyprland autoname workspaces"
    UpdateWorspaces

    InstallSection "Kitty"
    UpdateKitty

    InstallSection "Hypr"
    UpdateHyprland

    InstallSection "LSD"
    UpdateLsd

    InstallSection "Wofi"
    UpdateWofi

    InstallSection "Waybar"
    UpdateWaybar

    InstallSection "zsh"
    UpdateZsh

    InstallSection "Fastfetch"
    UpdateFastFetch

    InstallSection "Scripts"
    UpdateScripts
}

Main